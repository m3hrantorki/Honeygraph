import Config


class PrepareGenerators:

    """ Prepare signature generators """

    def __init__(self, training_stream_path):
        self.training_stream_path = training_stream_path

    def get_generators(self, signature_names):

        signature_generators = []

        for signature in signature_names:
            if signature == 'LCS':
                import polygraph.sig_gen.lcs
                signature_generators.append(polygraph.sig_gen.lcs.LCS(
                    pname="Longest Substring",
                    fname="LCS"
                ))
            elif signature == 'BayesAnd':
                import polygraph.sig_gen.bayes
                signature_generators.append(polygraph.sig_gen.bayes.Bayes(
                    pname="Conjunction",
                    fname="BayesAnd",
                    kfrac=1,
                    minlen=2,
                    training_trace=self.training_stream_path,
                    threshold_style='min'
                ))
            elif signature == 'BayesFuzzyAnd':
                import polygraph.sig_gen.bayes
                signature_generators.append(polygraph.sig_gen.bayes.Bayes(
                    pname="BayesFuzzyAnd",
                    fname="BayesFuzzyAnd",
                    kfrac=1,
                    minlen=2,
                    threshold_style='bound',
                    max_fpos=5,
                    training_trace=self.training_stream_path
                ))
            elif signature == 'LCSeq':
                import polygraph.sig_gen.lcseq_tree
                signature_generators.append(polygraph.sig_gen.lcseq_tree.LCSeqTree(
                    pname="Token Subsequence",
                    fname="lcseq",
                    kfrac=1,
                    tokenize_all=True,
                    tokenize_pairs=False,
                    minlen=2,
                    do_cluster=False
                ))
            elif signature == 'Bayes2':
                import polygraph.sig_gen.bayes
                signature_generators.append(polygraph.sig_gen.bayes.Bayes(
                    pname="Bayes2",
                    fname="bayes2",
                    kmin=4,
                    kfrac=.2,
                    minlen=2,
                    threshold_style='bound',
                    max_fpos=5,
                    training_trace=self.training_stream_path
                ))
            elif signature == 'Bayes':
                import polygraph.sig_gen.bayes
                signature_generators.append(polygraph.sig_gen.bayes.Bayes(
                    pname="Bayes",
                    fname="bayes",
                    kmin=3,
                    kfrac=.2,
                    minlen=2,
                    threshold_style='bound',
                    max_fpos=5,
                    training_trace=self.training_stream_path
                ))
            elif signature == 'LCSeqTree':
                import polygraph.sig_gen.lcseq_tree
                signature_generators.append(polygraph.sig_gen.lcseq_tree.LCSeqTree(
                    pname="Token Subsequence",
                    fname="lcseq_tree",
                    k=3,
                    tokenize_all=True,
                    tokenize_pairs=False,
                    minlen=2,
                    spec_threshold=3,
                    max_fp_count=5,
                    fpos_training_streams=self.training_stream_path,
                    min_cluster_size=3
                ))
            elif signature == 'BayesAndTree':
                import polygraph.sig_gen.bayes_tree
                signature_generators.append(polygraph.sig_gen.bayes_tree.BayesTree(
                    pname="Conjunction",
                    fname="BayesAndTree",
                    kfrac=1,
                    minlen=2,
                    threshold_style='min',
                    spec_threshold=3,
                    max_fp_count=5,
                    fpos_training_streams=self.training_stream_path,
                    min_cluster_size=3
                ))
            elif signature == 'BayesAndTree2':
                import polygraph.sig_gen.bayes_tree
                signature_generators.append(polygraph.sig_gen.bayes_tree.BayesTree(
                    pname="Conjunction2",
                    fname="BayesAndTree2",
                    kfrac=1,
                    minlen=2,
                    threshold_style='min',
                    spec_threshold=3,
                    max_fp_count=5,
                    fpos_training_streams=self.training_stream_path,
                    min_cluster_size=10
                ))
            elif signature == 'BayesFuzzyAndTree':
                import polygraph.sig_gen.bayes_tree
                signature_generators.append(polygraph.sig_gen.bayes_tree.BayesTree(
                    pname="BayesFuzzyAndTree",
                    fname="BayesFuzzyAndTree",
                    kfrac=1,
                    minlen=2,
                    threshold_style='bound',
                    spec_threshold=3,
                    max_fp_count=5,
                    fpos_training_streams=self.training_stream_path,
                    min_cluster_size=3
                ))
            elif signature == 'BayesTree':
                import polygraph.sig_gen.bayes_tree
                signature_generators.append(polygraph.sig_gen.bayes_tree.BayesTree(
                    pname="Bayes",
                    fname="bayes_tree",
                    kmin=3,
                    kfrac=.2,
                    minlen=2,
                    threshold_style='bound',
                    spec_threshold=3,
                    max_fp_count=5,
                    fpos_training_streams=self.training_stream_path,
                    min_cluster_size=3))
            else:
                Config.logger.error('Ignoring bad signature name: %s' % signature)

        return signature_generators
