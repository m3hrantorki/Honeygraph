SERVER_IP = '127.0.0.10'
SERVER_PORT = 80

CLIENT_IP_RANGE = (15, 220)  # must exclude server ip
CLIENT_PORT_RANGE = (1024, 65535)

TEST_TYPE = 2

NOISE_POOL_RANGES = [
    [0],  # for test type 1, atphttpd
    [0],  # for test type 2, codered
    [0, 2, 3, 4] + range(5, 31, 5) + [40, 50],  # for test type 3
    [0, 2, 3, 4] + range(5, 31, 5) + [40, 50],  # for test type 4
]

WORM_POOL_RANGES = [
    [3, 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100],  # for test type 1, atphttpd
    [3, 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100],  # for test type 2, codered
    [5],  # for test type 3
    [5],  # for test type 4
]
