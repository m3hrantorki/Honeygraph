import time
import random
import Config
import cPickle
from TcpConnection import TcpConnection

codered_pickle = open('pickles/codered.pickle')
codered_samples = cPickle.load(codered_pickle)

http_pickle = open('pickles/http.pickle')
http_samples = cPickle.load(http_pickle)

atp_pickle = open('pickles/atphttpd.pickle')
atp_samples = cPickle.load(atp_pickle)

noise_ranges = Config.NOISE_POOL_RANGES[Config.TEST_TYPE-1]
worm_ranges = Config.WORM_POOL_RANGES[Config.TEST_TYPE-1]

for worm_size in worm_ranges:
    for noise_size in noise_ranges:
        src_ip = '127.0.0.%d' % random.randint(Config.CLIENT_IP_RANGE[0], Config.CLIENT_IP_RANGE[1])
        src_port = random.randint(Config.CLIENT_PORT_RANGE[0], Config.CLIENT_PORT_RANGE[1])

        connection = TcpConnection(
            src_ip=src_ip,
            src_port=src_port,
            dest_ip=Config.SERVER_IP,
            dest_port=Config.SERVER_PORT)

        connection.connect()

        random.shuffle(atp_samples)
        random.shuffle(codered_samples)
        random.shuffle(http_samples)

        if Config.TEST_TYPE != 2:
            worm_samples = atp_samples[:worm_size]
        else:
            worm_samples = codered_samples[:worm_size]

        if Config.TEST_TYPE == 4:
            worm_samples.extend(codered_samples[:worm_size])

        noise_samples = http_samples[:noise_size]

        all_samples = worm_samples + noise_samples

        for sample in all_samples:
            connection.send(sample)
            time.sleep(0.2)

        time.sleep(0.5)

        connection.close()
