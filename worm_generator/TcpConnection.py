import socket


class TcpConnection:
    def __init__(self, src_ip, src_port, dest_ip, dest_port):
        self.src_ip = src_ip
        self.src_port = src_port
        self.dest_ip = dest_ip
        self.dest_port = dest_port

        self.tcp_socket = None

    def connect(self):
        self.tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.tcp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.tcp_socket.bind((self.src_ip, self.src_port))
        self.tcp_socket.connect((self.dest_ip, self.dest_port))

        pass

    def send(self, data):
        self.tcp_socket.send(data)

    def close(self):
        self.tcp_socket.shutdown(1)
        self.tcp_socket.close()
