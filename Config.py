""" Configuration file """

import logging

""" Log Configuration """

LOG_DIR = 'logs/'

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
console_handler.setFormatter(formatter)

debug_handler = logging.FileHandler(LOG_DIR + 'debug.log')
debug_handler.setLevel(logging.DEBUG)
debug_handler.setFormatter(formatter)

info_handler = logging.FileHandler(LOG_DIR + 'info.log')
info_handler.setLevel(logging.INFO)
info_handler.setFormatter(formatter)

warning_handler = logging.FileHandler(LOG_DIR + 'warning.log')
warning_handler.setLevel(logging.DEBUG)
warning_handler.setFormatter(formatter)

error_handler = logging.FileHandler(LOG_DIR + 'error.log')
error_handler.setLevel(logging.DEBUG)
error_handler.setFormatter(formatter)

logger.addHandler(debug_handler)
logger.addHandler(info_handler)
logger.addHandler(warning_handler)
logger.addHandler(error_handler)
logger.addHandler(console_handler)

""" Innocuous pool configs """
INNOCUOUS_TRACES_BASE = '/home/m3hran/Projects/BS_Project/Sniff/innocuous_traces/'
TRAINING_TRACE_FILE_NAME = 'training'
EVALUATION_TRACE_FILE_NAME = 'eval'  # for false positives evaluation

""" Suspicious pool configs """
SUSPICIOUS_TRACES_BASE = '/home/m3hran/Projects/BS_Project/Sniff/suspicious_traces/'

""" Sniffer Configuration """
MAX_BYTES_PER_PACKET = 65536
PROMISCUOUS_MODE = 1
SNIFF_TIME_OUT = 0

""" HoneyGraph Configuration """
SNIFF_PERIOD = 1  # in minute
SNIFF_TIME_IN_SECONDS = SNIFF_PERIOD * 60
TEST_SIGNATURES_NAMES = ['BayesAndTree', 'LCSeqTree', 'Bayes2', 'LCS']
