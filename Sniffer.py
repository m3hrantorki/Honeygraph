import threading
import Config
import pcapy


class Sniffer:

    """ Sniffs packets on input interface and saves packets to pcap file """

    def __init__(self, device, sniff_lock):

        self.finish = False
        self.device = device
        self.sniff_lock = sniff_lock

    def start_sniff(self, path):

        self.finish = False

        thread = threading.Thread(target=self.run, args=(path,))
        thread.daemon = True
        thread.start()

    def run(self, path):

        Config.logger.debug('Sniffer started on %s' % self.device)

        cap = pcapy.open_live(
            self.device,
            Config.MAX_BYTES_PER_PACKET,
            Config.PROMISCUOUS_MODE,
            Config.SNIFF_TIME_OUT
        )

        # cap.setfilter('tcp')

        dumper = cap.dump_open(path)

        count = 0
        while not self.finish:

            (header, packet) = cap.next()

            count += 1

            Config.logger.debug('%d: captured %d bytes, truncated to %d bytes'
                                % (count, header.getlen(), header.getcaplen()))

            dumper.dump(header, packet)

        Config.logger.debug('Sniff period finished .....')

        self.sniff_lock.release()

    def stop_sniff(self):
        self.finish = True
