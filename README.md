# Honeygraph

Honeygraph is network traffic sniffer that is designed to run on a honeypot. Honeygraph captures honeypot's traffic in a period of time, then reassmbles TCP flows and sends each flow to Polygraph system to generate signatures.

## Getting Started

### Prerequisites

Polygraph must be installed. First download polygraph from [here](https://github.com/sporksmith/polygraph). Extract it and then:
```
cd polygraph-master
```
Install polygraph:

```
python setup.py
```
### Running Honeygraph

```
python HoneyGraph.py
```