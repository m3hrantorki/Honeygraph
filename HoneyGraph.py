import os
import time
import pcapy
import Config
import threading
from Sniffer import Sniffer
from PcapStream import PcapStream
from PcapSignatureGenerator import PcapSignatureGenerator


class HoneyGraph:

    """ Sniffs selected interface for a period of time
    and runs PolyGraph algorithm on generated pcap file """

    def __init__(self):

        Config.logger.info('Starting parsing innocuous traces')
        PcapStream(Config.INNOCUOUS_TRACES_BASE, Config.TRAINING_TRACE_FILE_NAME).start()
        PcapStream(Config.INNOCUOUS_TRACES_BASE, Config.EVALUATION_TRACE_FILE_NAME).start()

        # list all devices
        devices = pcapy.findalldevs()

        for index, device in enumerate(devices):
            print '%d. %s' % (index+1, device)

        index = raw_input("\nEnter device index to start sniffing : ")
        device = devices[int(index)-1]

        print "\nSniffing device " + device + ' ...... \n'

        self.sniff_lock = threading.Lock()
        self.sniffer = Sniffer(device, self.sniff_lock)

        Config.logger.info('Created sniffer object on %s interface' % device)

    def stop_sniffer(self):
        Config.logger.debug('Stopping sniffer')
        self.sniffer.stop_sniff()

    def start(self):

        while True:

            timestamp = int(time.time())

            directory = str(timestamp) + '/'
            file_name = 'result'
            path = Config.SUSPICIOUS_TRACES_BASE + directory + file_name + '.pcap'

            Config.logger.debug('Trying to create directory %s' % directory)
            try:
                os.mkdir(Config.SUSPICIOUS_TRACES_BASE + directory)
                Config.logger.debug('Directory %s created' % directory)
            except OSError:
                Config.logger.error('Failed to create directory %s, exiting....' % directory)
                break

            timer = threading.Timer(Config.SNIFF_TIME_IN_SECONDS, self.stop_sniffer)
            timer.start()
            Config.logger.debug('Timer started for other %d minutes' % Config.SNIFF_PERIOD)

            self.sniff_lock.acquire()

            self.sniffer.start_sniff(path)

            try:
                self.sniff_lock.acquire()
            except KeyboardInterrupt:
                Config.logger.info('User break')
                break

            Config.logger.debug('Signature generation for pcap file started .... ')

            PcapSignatureGenerator(
                test_signatures=Config.TEST_SIGNATURES_NAMES,
                test_directory=directory,
                test_file_name=file_name,
                daemon=False
            ).start()

            self.sniff_lock.release()

if __name__ == '__main__':
    HoneyGraph().start()

