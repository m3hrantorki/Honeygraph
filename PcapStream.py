import os
import struct
import Config
import polygraph.trace_crunching.pkts_to_streams as pkts_to_streams


class PcapStream:

    """ Creates data, offset and sary file from pcap file """

    def __init__(self, file_path, file_name):

        self.file_path = file_path
        self.file_name = file_name
        self.offset = 0

    def start(self):
        path_to_file = self.file_path + self.file_name
        pcap_path = path_to_file + '.pcap'
        streams_dirs = path_to_file + '.streams'

        if os.path.isdir(streams_dirs):
            Config.logger.info('Directory %s exists, exiting' % streams_dirs)
            return

        try:
            os.mkdir(streams_dirs)
            Config.logger.info('Directory %s created' % streams_dirs)
        except OSError:
            Config.logger.error('Failed to create directory "%s"' % streams_dirs)
            return

        data_file = open(streams_dirs + '/data', 'w')
        offset_file = open(streams_dirs + '/offsets', 'w')

        def callback(stream):
            if len(stream['data']) > 0:
                offset_file.write(struct.pack('L', self.offset))
                data_file.write(stream['data'])
                self.offset += len(stream['data'])

        pkts_to_streams.process_trace(pcap_path, callback=callback, timeout=600, filter=None)

        os.popen2('mksary %s' % (streams_dirs + '/data'))
