import time
import pcapy
import Config
import threading
import impacket.ImpactPacket
import impacket.ImpactDecoder
from PrepareGenerators import PrepareGenerators
from polygraph.trace_crunching.stream_trace import StreamTrace


class PcapSignatureGenerator(threading.Thread):

    """ Runs the test on input pcap file """

    def __init__(self, test_signatures, test_directory, test_file_name, daemon=False):

        threading.Thread.__init__(self)

        self.test_signatures = test_signatures
        self.test_directory = test_directory
        self.test_file_name = test_file_name
        self.daemon = daemon

        training_path = Config.INNOCUOUS_TRACES_BASE + Config.TRAINING_TRACE_FILE_NAME + '.streams'
        self.signature_generators = PrepareGenerators(training_path).get_generators(self.test_signatures)

        self.signature_file = None

    def run(self):

        suspicious_trace_path = Config.SUSPICIOUS_TRACES_BASE + self.test_directory

        self.signature_file = open(suspicious_trace_path + 'signatures.txt', 'w')

        self.process_trace(
            trace_name=suspicious_trace_path + self.test_file_name + '.pcap',
            callback=self.handle_stream,
            filters=None,
            save_data=True,
            timeout=150
        )

        self.signature_file.close()

        Config.logger.debug('Signature generation for pcap file finished .... ')

    @staticmethod
    def get_next_packet(tracer, save_data):

        # set up decoder
        assert (tracer.datalink() == pcapy.DLT_EN10MB)  # assuming ethernet
        decoder = impacket.ImpactDecoder.EthDecoder()

        # loop until we've got a processable packet
        while True:
            try:
                packet_info, packet = tracer.next()
            except pcapy.PcapError:
                return None

            if not packet_info:
                return None

            # extract basic capture info
            if packet_info.getlen() != packet_info.getcaplen():
                Config.logger.warning("Warning, only captured %d of %d bytes" % (
                    packet_info.getcaplen(), packet_info.getlen()))

            # reconstruct time stamp
            ts_s, ts_us = packet_info.getts()
            ts = ts_s + (ts_us / 1.0e6)

            # parse the packet
            packet_link = decoder.decode(packet)

            # discard ethernet frame
            packet_net = packet_link.child()

            # skip non-IP packets
            if not isinstance(packet_net, impacket.ImpactPacket.IP):
                continue

            # strip ip header
            packet_transport = packet_net.child()

            if isinstance(packet_transport, impacket.ImpactPacket.TCP):
                src_port = packet_transport.get_th_sport()
                dst_port = packet_transport.get_th_dport()
                transport_type = "tcp"
                if packet_transport.get_RST():
                    status = "rst"
                elif packet_transport.get_FIN():
                    status = "fin"
                else:
                    status = "open"
            elif isinstance(packet_transport, impacket.ImpactPacket.UDP):
                src_port = packet_transport.get_uh_sport()
                dst_port = packet_transport.get_uh_dport()
                transport_type = "udp"
                status = "udp"
            else:  # not tcp or udp, grab the next packet instead
                continue
            break  # got a tcp or udp packet, process and return

        # src ip, dst ip, src port, dst port
        connection = (packet_net.get_ip_src(), packet_net.get_ip_dst(),
                      src_port, dst_port)

        packet = {
            'connection': connection,
            'timestamps': [ts],
            'type': transport_type,
            'status': status,
            'num_of_packets': 1,
            'data': []
        }

        if save_data and packet_transport.get_data_as_string():
            packet['data'] = [packet_transport.get_data_as_string()]

        return packet

    def process_trace(self, trace_name, callback, timeout=2000, filters=None, save_data=True):
        """
        Process all the streams in the libpcap file.

        trace_name is the name of the libpcap file to open and process.
        callback should take a stream as its only positional argument.
        timeout is the number of seconds without seeing a packet before
        considering a connection closed and processing the corresponding
        stream.

        The stream passed to callback is a dictionary with the following keys:
        connection: (src ip, dst ip, src port, dst port)
        data: string containing the reassembled stream
        timestamps: sequence of packet time stamps
        num_of_packets: packet count

        callback may raise an IndexError exception to halt processing
        of the trace.
        """

        streams = []  # stream q, most recent at 0
        tracer = pcapy.open_offline(trace_name)

        if filters:
            tracer.setfilter(filters)

        while True:
            packet = self.get_next_packet(tracer, save_data)
            if not packet:
                break

            now = packet["timestamps"][-1]

            # process udp packets individually
            if packet["type"] == "udp":
                try:
                    callback(packet)
                except IndexError:
                    return
                continue

            for i in xrange(len(streams)):
                stream = streams[i]
                if stream["connection"] == packet["connection"]:
                    # already have packets in this connection.
                    # update and process if connection is closed,
                    # or put at beginning of q (if connection still open)
                    stream["data"].extend(packet["data"])
                    stream["status"] = packet["status"]
                    stream["timestamps"].append(packet["timestamps"][-1])
                    stream['num_of_packets'] += 1
                    del (streams[i])
                    if stream["status"] == "fin" or stream["status"] == "rst":
                        try:
                            callback(stream)
                        except IndexError:
                            return
                    else:
                        streams.insert(0, stream)
                    break
            else:
                # new connection, insert into q if data length > 0
                if len(packet["data"]) > 0:
                    Config.logger.debug('New connection: %s' % str(packet['connection']))
                    streams.insert(0, packet)

            # process any timed out connections
            while len(streams) > 0 and now - streams[-1]["timestamps"][-1] > timeout:
                # remove from q
                stream = streams[-1]
                del (streams[-1])
                stream["status"] = "timeout"
                try:
                    Config.logger.debug('Connection %s passed timeout %d'
                                        % (str(stream['connection']), timeout))
                    callback(stream)
                except IndexError:
                    return

        # no more packets, finish processing streams
        while len(streams) > 0:
            # remove from q
            stream = streams[-1]
            del (streams[-1])
            try:
                callback(stream)
            except IndexError:
                return

    def log_signature(self, data):
        self.signature_file.write(data)
        self.signature_file.flush()

    @staticmethod
    def test_false_negatives(signature, trace):
        false_negatives = 0
        count = 0
        for sample in trace:
            count += 1
            if not signature.match(sample):
                false_negatives += 1
        return false_negatives, count

    @staticmethod
    def test_false_positives(signature, trace):
        false_positives = 0

        count = 0
        s = StreamTrace(trace)

        sample = s.next()
        while sample:
            count += 1
            if signature.match(sample):
                false_positives += 1
            sample = s.next()

        return false_positives, count

    def handle_stream(self, stream):

        if len(stream['data']) > 0:

            self.log_signature('Connection: %s\n' % str(stream['connection']))

            for sig in self.signature_generators:

                self.log_signature('\nSignature: %s [function name: %s], ' % (sig.pname, sig.fname))

                t0 = time.time()

                try:
                    cluster_signatures = sig.train(stream['data'])
                except AssertionError:
                    Config.logger.error('Connection %s has %d samples, not enough!'
                                        % (str(stream['connection']), len(stream['data'])))
                    continue

                t1 = time.time()
                delta_t = t1-t0

                self.log_signature('generated in %f seconds, with pool size of %d samples'
                                   % (delta_t, len(stream['data'])))

                for cluster_sig in cluster_signatures:

                    self.log_signature('\n' + str(cluster_sig))

                    # test false negatives
                    trace = stream['data']
                    false_negatives, count = self.test_false_negatives(cluster_sig, trace)
                    self.log_signature('\n\t false negatives: %d out of %d'
                                       % (false_negatives, count))

                    # test false positives
                    evaluation_trace = \
                        Config.INNOCUOUS_TRACES_BASE + Config.EVALUATION_TRACE_FILE_NAME + '.streams'
                    false_positives, count = self.test_false_positives(cluster_sig, evaluation_trace)
                    self.log_signature('\n\t false positives: %d out of %d\n'
                                       % (false_positives, count))

                self.log_signature('\n')

            self.log_signature('\n' + 80*'#' + '\n')

if __name__ == '__main__':
    PcapSignatureGenerator(
        test_signatures=Config.TEST_SIGNATURES_NAMES,
        test_directory='TEST_3/',
        test_file_name='test_3',
        daemon=False
    ).start()
